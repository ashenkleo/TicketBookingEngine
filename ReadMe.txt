-Instructions-

01. Get the project to local repositary
git clone https://gitlab.com/ashenkleo/TicketBookingEngine.git

02. Pulling the latest changes to local
git pull

03. Pushing your changes
	01. git status (check the changes)
	02. git add <file path> (to add files one by one)
		or git add . (to add all files at once) 
	03. git commit -m "<message>"
	04. git pull (pull the latest)
	05. git push


